package com.example.opengltest2;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import android.content.res.Resources;
import android.opengl.GLES30;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import de.javagl.obj.Obj;
import de.javagl.obj.ObjData;
import de.javagl.obj.ObjReader;
import de.javagl.obj.ObjUtils;

public class MyGLRenderer implements GLSurfaceView.Renderer
{
    private ArrayList<Triangle> objs;

    private Resources resources;

    private int mProgram;

    private float[] viewMatrix = new float[16];
    private float[] projectionMatrix = new float[16];

    final float[] triangleData = {
            // X, Y, Z,
            // R, G, B, A
            -0.5f, 0.5f, 0.0f,
            1.0f, 0.0f, 0.0f, 1.0f,

            0.5f, 0.5f, 0.0f,
            0.0f, 1.0f, 0.0f, 1.0f,

            0.5f, -0.5f, 0.0f,
            0.0f, 0.0f, 1.0f, 1.0f,

            -0.5f, -0.5f, 0.0f,
            1.0f, 1.0f, 1.0f, 1.0f,
    };

    private short[] indexData = {
            0,1,2,
            2,3,0
    };

    public MyGLRenderer(Resources r)
    {
        this.resources = r;
    }

    public void onSurfaceCreated(GL10 unused, EGLConfig config)
    {
        final float eyeX = 0.0f;
        final float eyeY = 2.0f;
        final float eyeZ = 0.0f;

        // We are looking toward the distance
        final float lookX = 0.0f;
        final float lookY = 0.0f;
        final float lookZ = 0.0f;

        // Set our up vector. This is where our head would be pointing were we holding the camera.
        final float upX = 0.0f;
        final float upY = 0.0f;
        final float upZ = 1.0f;

        Matrix.setLookAtM(viewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);
        //Up vector is what the camera considers "up", i.e.: If you were looking forward and held your hand up, that is your "up" vector. Just set it to 0, 1, 0

        init();

        objs = new ArrayList<>();
        
        Obj tempObj = LoadObj();

        objs.add(new Triangle(ObjData.getVertices(tempObj), ObjData.getFaceVertexIndices(tempObj)));
        objs.add(new Triangle(ObjData.getVertices(tempObj), ObjData.getFaceVertexIndices(tempObj)));
    }

    private Obj LoadObj()
    {
        InputStream objInputStream = resources.openRawResource(R.raw.andy);
        Obj obj = null;
        try {
            obj = ObjReader.read(objInputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        obj = ObjUtils.convertToRenderable(obj);

        return obj;
    }

    public void onDrawFrame(GL10 unused)
    {
        GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT);

        GLES30.glUseProgram(mProgram);

        objs.get(0).draw(viewMatrix, projectionMatrix, 0);
        objs.get(1).draw(viewMatrix, projectionMatrix, 1);
    }

    public void onSurfaceChanged(GL10 unused, int width, int height)
    {
        GLES30.glViewport(0, 0, width, height);

        final float ratio = (float) width / height;
        final float left = -ratio;
        final float right = ratio;
        final float bottom = -1.0f;
        final float top = 1.0f;
        final float near = 1.0f;
        final float far = 10.0f;

        Matrix.frustumM(projectionMatrix, 0, left, right, bottom, top, near, far);
        //View frustum is just a visual representation of perspective projection that is used to convert 3D point in the world coordinate space to the 2D point on the screen.
    }

    private static int loadShader(int type, String shaderCode)
    {
        int shader = GLES30.glCreateShader(type);

        GLES30.glShaderSource(shader, shaderCode);
        GLES30.glCompileShader(shader);

        return shader;
    }

    private String loadStringFromAssetFile(int fileId)
    {
        StringBuilder shaderSource = new StringBuilder();

        try
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(resources.openRawResource(fileId)));
            String line;

            while((line = reader.readLine()) != null)
            {
                shaderSource.append(line).append("\n");
            }
            reader.close();
            return shaderSource.toString();

        }

        catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    private void init()
    {
        String vertexShaderCode = loadStringFromAssetFile(R.raw.vertex_shader);
        String fragmentShaderCode = loadStringFromAssetFile(R.raw.fragment_shader);

        int vertexShader = MyGLRenderer.loadShader(GLES30.GL_VERTEX_SHADER,
                vertexShaderCode);
        int fragmentShader = MyGLRenderer.loadShader(GLES30.GL_FRAGMENT_SHADER,
                fragmentShaderCode);

        mProgram = GLES30.glCreateProgram();

        GLES30.glShaderSource(vertexShader, vertexShaderCode);
        GLES30.glShaderSource(fragmentShader, fragmentShaderCode);

        GLES30.glCompileShader(vertexShader);
        GLES30.glCompileShader(fragmentShader);

        GLES30.glAttachShader(mProgram, vertexShader);
        GLES30.glAttachShader(mProgram, fragmentShader);

        GLES30.glEnableVertexAttribArray(0);
        GLES30.glEnableVertexAttribArray(1);
        GLES30.glEnableVertexAttribArray(2);

        GLES30.glLinkProgram(mProgram);
    }
}