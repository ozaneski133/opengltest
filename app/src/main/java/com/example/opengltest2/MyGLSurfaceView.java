package com.example.opengltest2;

import android.content.Context;
import android.opengl.GLSurfaceView;

class MyGLSurfaceView extends GLSurfaceView
{
    private final MyGLRenderer renderer;

    public MyGLSurfaceView(Context context)
    {
        super(context);

        setEGLContextClientVersion(3);

        renderer = new MyGLRenderer(getResources());

        setRenderer(renderer);
    }
}