package com.example.opengltest2;

import android.opengl.GLES30;
import android.opengl.Matrix;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

public class Triangle
{
    private final int BYTES_PER_FLOAT = 4;
    private final int STRIDE = 7 * BYTES_PER_FLOAT;
    private static final int BYTES_PER_INT = 4;
    private final int POSITION_OFFSET = 0;
    private final int POSITION_DATA_SIZE = 3;
    private final int mvpMatrixHandle = 2;

    private int VAO;

    private int vertexVBO;
    private int indexVBO;

    private int indexCount;

    private static final int POSITION_HANDLE = 1;

    float[] modelMatrix = new float[16];

    public Triangle(FloatBuffer vertexData, IntBuffer indexData)
    {
        Matrix.setIdentityM(modelMatrix, 0);
        Matrix.scaleM(modelMatrix, 0, 5, 5, 5);

        this.indexCount = indexData.capacity();

        // Create buffer objects
        int[] vbos = new int[2];
        GLES30.glGenBuffers(2, vbos, 0);
        vertexVBO = vbos[0];
        indexVBO = vbos[1];

        int[] vaos = new int[1];
        GLES30.glGenVertexArrays(1, vaos, 0);
        VAO = vaos[0];

        GLES30.glBindVertexArray(VAO);

        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, vertexVBO);
        GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER, vertexData.capacity() * BYTES_PER_FLOAT,
                vertexData, GLES30.GL_STATIC_DRAW);

        GLES30.glVertexAttribPointer(POSITION_HANDLE, POSITION_DATA_SIZE, GLES30.GL_FLOAT, false, STRIDE, POSITION_OFFSET * BYTES_PER_FLOAT);
        GLES30.glEnableVertexAttribArray(1);

        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, indexVBO);
        GLES30.glBufferData(GLES30.GL_ELEMENT_ARRAY_BUFFER, indexData.capacity() * BYTES_PER_INT,
                indexData, GLES30.GL_STATIC_DRAW);

        GLES30.glBindVertexArray(0);
    }

    public void draw(float[] viewMatrix, float[] projectionMatrix, int rotation)
    {
        switch (rotation)
        {
            case 0:
                rotateX();
                break;
            case 1:
                rotateY();
                break;
            case 2:
                rotateZ();
        }

        float[] mMVPMatrix = new float[16];
        Matrix.multiplyMM(mMVPMatrix, 0, viewMatrix, 0, modelMatrix, 0);

        Matrix.multiplyMM(mMVPMatrix, 0, projectionMatrix, 0, mMVPMatrix, 0);

        GLES30.glUniformMatrix4fv(mvpMatrixHandle, 1, false, mMVPMatrix, 0);

        GLES30.glBindVertexArray(VAO);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, this.indexCount, GLES30.GL_UNSIGNED_INT, 0);
        GLES30.glBindVertexArray(0);
    }

    public void rotateX()
    {
        Matrix.rotateM(modelMatrix, 0, 0.1f, 1.0f, 0.0f, 0.0f);
    }

    public void rotateY()
    {
        Matrix.rotateM(modelMatrix, 0, 0.1f, 0.0f, 1.0f, 0.0f);
    }

    public void rotateZ()
    {
        Matrix.rotateM(modelMatrix, 0, 0.1f, 0.0f, 0.0f, 1.0f);
    }
}