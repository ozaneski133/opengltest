#version 300 es

layout(location = 2) uniform mat4 u_MVPMatrix;
layout(location = 1) in vec3 a_Position;
layout(location = 0) in vec4 a_Color;

out vec4 vColor;

void main()
{
  vColor = a_Color;
  gl_Position = u_MVPMatrix * vec4(a_Position, 1);
}